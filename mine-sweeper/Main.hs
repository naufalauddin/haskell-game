{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE OverloadedRecordDot #-}
{-# LANGUAGE DerivingStrategies #-}
{-# OPTIONS_GHC -Wno-unused-top-binds #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE LambdaCase #-}
module Main ( main ) where

import Type

import Raylib.Core
import Raylib.Core.Text
import Raylib.Util
import Raylib.Util.Colors
import Raylib.Types
import Raylib.Core.Shapes
import Raylib.Core.Textures
import Raylib.Util.Math
import Control.Monad.State
import Control.Monad.Catch
import Control.Lens
import qualified Data.Vector as V
import System.Random (mkStdGen, randomIO, StdGen)
import System.Random.Stateful (newIOGenM, StatefulGen, uniformListM, IOGenM)
import Data.Foldable
import Data.List (sortBy, intersect, (\\))
import Data.Ord (comparing)
import Data.Set (Set)
import qualified Data.Set as Set

main :: IO ()
main = do
  initialGameState <- initGameState
  (`evalStateT` initialGameState) $ withWindow (round viewportWidth) (round viewportHeight) "Sweep" 60 \_ -> do
    liftIO $ setWindowState [WindowResizable]
    whileWindowOpen0 do
      handleCamera
      playing <- uses winningState (== Playing)
      when playing handleGamePlay
      handleZoom
      handleReset
      handleTick
      checkWinCondition
      drawGame

handleCamera
  :: ( MonadState s m
     , MonadIO m
     , HasZoomFactor s Float
     , HasCamera s Camera2D
     , HasMousePosition s Vector2
     )
  => m ()
handleCamera = do
  screenHeight <- liftIO getScreenHeight
  screenWidth <- liftIO getScreenWidth
  zoomFactor' <- use zoomFactor
  let cameraOffset = Vector2 { vector2'y = fromIntegral screenHeight/2, vector2'x = fromIntegral screenWidth / 2 }
      cameraZoom = min (fromIntegral screenWidth / viewportWidth) (fromIntegral screenHeight / viewportHeight) * zoomFactor'
  camera .= Camera2D { camera2D'zoom = cameraZoom, camera2D'target = zero, camera2D'rotation = 0, camera2D'offset = cameraOffset }
  mousePos <- liftIO getMousePosition
  mousePosition .= (mousePos |-| cameraOffset) |/ cameraZoom

handleGamePlay
  :: ( MonadState s m
     , HasMousePosition s Vector2
     , HasGameBoard s (Grid Tile)
     , HasAdjacentTiles s (Coord -> [Coord])
     , MonadIO m
     )
  => m ()
handleGamePlay = do
  leftClick <- liftIO $ isMouseButtonPressed MouseButtonLeft
  rightClick <- liftIO $ isMouseButtonPressed MouseButtonRight
  middleClick <- liftIO $ isMouseButtonPressed MouseButtonMiddle
  (Vector2 mouseX mouseY) <- use mousePosition
  board <- use gameBoard
  neighbor <- use adjacentTiles
  let
    BoardSizes{..} = boardSizes board
    clickedRow = floor ((mouseY + boardHeight/2) / tileSize)
    clickedCol = floor ((mouseX + boardWidth/2) / tileSize)
    clickedCoord = Coord { row = clickedRow, col = clickedCol }
  when (leftClick && anyOf (ix clickedCoord . face) (== Hidden) board) do
    gameBoard %= openBoard [clickedCoord] neighbor
  when (middleClick && anyOf (ix clickedCoord . face) (== Shown) board) do
    case board ^? ix clickedCoord . tileType of
      Just (Number n) -> do
        let flaggedNeighbor = filter (\i -> (board ^? ix i . face) == Just Flag) neighboringTiles
            neighboringTiles = neighbor clickedCoord
        when (length flaggedNeighbor == n) do
          gameBoard %= openBoard (neighboringTiles \\ flaggedNeighbor ) neighbor
      _ -> pure ()
  when rightClick do
    gameBoard . ix clickedCoord . face %= \case
      Flag -> Hidden
      Hidden -> Flag
      Shown -> Shown

openBoard :: [Coord] -> (Coord -> [Coord]) -> Grid Tile -> Grid Tile
openBoard c = go Set.empty (Set.fromList c)
  where
    go :: Set Coord -> Set Coord -> (Coord -> [Coord]) -> Grid Tile -> Grid Tile
    go visited queue neighbors currentBoard
      | Set.null queue = currentBoard
      | otherwise =
        let openedUpBoard = currentBoard & ix coord . filtered (anyOf face (/= Flag)) . face .~ Shown
            (coord, pending) = Set.deleteFindMin queue
        in
        case currentBoard ^? ix coord . tileType of
          Nothing -> go (Set.insert coord visited) pending neighbors currentBoard
          Just Mine -> currentBoard & mapped . filtered (\Tile {_tileType} -> _tileType == Mine) . face .~ Shown
          Just Blank ->
            let neigboring = Set.fromList (neighbors coord) Set.\\ visited
            in go (Set.insert coord visited) (pending `Set.union` neigboring) neighbors openedUpBoard
          Just (Number _) -> go (Set.insert coord visited) pending neighbors openedUpBoard

checkWinCondition
  :: ( MonadState s m
     , HasWinningState s Cond
     , HasGameBoard s (Grid Tile)
     )
  => m ()
checkWinCondition = do
  board <- use gameBoard
  when (not (any mineShown board) && not (any nonMineHidden board)) do
    winningState .= Win
  when (any mineShown board) do
    winningState .= Lose
  where
    mineShown Tile { _tileType = Mine, _face = Shown } = True
    mineShown _ = False
    nonMineHidden Tile { _tileType, _face = Hidden } | _tileType /= Mine = True
    nonMineHidden _ = False

handleZoom
  :: ( MonadState s m
     , HasZoomFactor s Float
     , MonadIO m
     )
  => m ()
handleZoom = do
  wheelMovement <- liftIO getMouseWheelMove
  zoomFactor += wheelMovement
  zoomFactor %= max 1

handleReset
  :: ( MonadState s m
     , MonadIO m
     , HasGameSeed s (IOGenM StdGen)
     , HasGameBoard s (Grid Tile)
     , HasWinningState s Cond
     , HasGameDifficulty s Difficulty
     )
  => m ()
handleReset = do
  keyRPressed <- liftIO getKeyPressed <&> (== KeyR)
  when keyRPressed do
    resetGame

drawGame
  :: ( MonadState GameState m
     , MonadMask m
     , MonadIO m
     )
  => m ()
drawGame = do
  (Vector2 mouseX mouseY) <- use mousePosition
  camera2D <- use camera
  condition <- use winningState
  difficulty <- use gameDifficulty
  time <- use timeElapsed
  drawing do
    mode2D camera2D do
      liftIO $ clearBackground black
      liftIO $ drawRectangle (negate (round (viewportWidth/2))) (negate (round (viewportHeight/2))) (round viewportWidth) (round viewportHeight) rayWhite
      drawBoard
    liftIO $ drawText ("Mouse Position: " <> show mouseX <> ", " <> show mouseY) 10 30 18 green
    liftIO $ drawFPS 10 10
    liftIO $ drawText ("Condition: " <> show condition) 10 50 18 green
    liftIO $ drawText ("Difficulty: " <> show difficulty) 10 70 18 green
    liftIO $ drawText ("Time Elapsed: " <> show (round time :: Int)) 10 90 18 green

initGameState :: IO GameState
initGameState = do
  seed <- randomIO
  g <- newIOGenM (mkStdGen seed)
  let coords = [ Coord { row, col } | row <- [0..8], col <- [0..8]]
  let dumbCamera = Camera2D { camera2D'zoom = 1, camera2D'target = zero, camera2D'rotation = 0, camera2D'offset = zero }
  mines <- take 10 <$> shuffle g coords
  pure GameState
    { _mousePosition = zero
    , _zoomFactor = 1
    , _gameBoard = boardFromMines (Coord 9 9) adjacent mines
    , _gameSeed = g
    , _camera = dumbCamera
    , _winningState = Playing
    , _gameDifficulty = Beginner
    , _timeElapsed = 0
    , _adjacentTiles = adjacent
    }

resetGame
  :: ( MonadState s m
     , MonadIO m
     , HasGameSeed s (IOGenM StdGen)
     , HasGameBoard s (Grid Tile)
     , HasGameDifficulty s Difficulty
     , HasWinningState s Cond
     )
  => m ()
resetGame = do
  g <- use gameSeed
  diff <- use gameDifficulty
  let dimension = diffDimension diff
      numOfMines = diffNumberOfMine diff
      coords = [ Coord { row, col } | row <- [0..dimension.row - 1], col <- [0..dimension.col - 1]]
  mines <- take numOfMines <$> shuffle g coords
  gameBoard .= boardFromMines dimension adjacent mines
  winningState .= Playing

handleTick
  :: ( MonadState s m
     , HasTimeElapsed s Float
     , HasWinningState s Cond
     , MonadIO m
     )
  => m ()
handleTick = do
  delta <- liftIO getFrameTime
  playing <- uses winningState (== Playing)
  when playing do
    timeElapsed += delta

adjacent :: Coord -> [Coord]
adjacent Coord {row, col} = [ Coord {row = row + x, col = col + y} | x <- [-1,0,1], y <- [-1,0,1]]

boardFromMines :: Coord -> (Coord -> [Coord]) -> [Coord] -> Grid Tile
boardFromMines dimension neighboring mines =
  Grid $
    V.generate dimension.row \row ->
      V.generate dimension.col \col ->
        if Coord {row, col} `elem` mines
          then Tile Mine Hidden
          else Tile
           { _tileType = tileFromInt $ length $ neighboring (Coord {row, col}) `intersect` mines
           , _face = Hidden
           }

shuffle :: (StatefulGen g m) => g -> [a] -> m [a]
shuffle g list = do
  ns :: [Int] <- uniformListM (length list) g
  pure $ map snd $ sortBy (comparing fst) $ zip ns list

drawBoard
  :: ( MonadState s m
     , MonadIO m
     , HasGameBoard s (Grid Tile)
     )
  => m ()
drawBoard = do
  board <- use gameBoard
  let BoardSizes {..} = boardSizes board
      (numOfRow, numOfCol) = gridDimension board
  liftIO do
    drawRectangleRec boardRectangle gray
    for_ [0..numOfRow] \i -> do
      drawLineV
        (Vector2 {vector2'x = negate (boardWidth / 2), vector2'y = fromIntegral i * tileSize - boardHeight/2})
        (Vector2 {vector2'x = boardWidth / 2, vector2'y = fromIntegral i * tileSize - boardHeight/2})
        black
    for_ [0..numOfCol] \i -> do
      drawLineV
        (Vector2 {vector2'x = fromIntegral i * tileSize - boardWidth / 2, vector2'y = negate (boardHeight/2) })
        (Vector2 {vector2'x = fromIntegral i * tileSize - boardWidth / 2, vector2'y = boardHeight/2})
        black
    ifor_ board \coord tile -> do
      let origin = Vector2
            { vector2'x = (fromIntegral coord.col + 1/2) * tileSize - boardWidth/2
            , vector2'y = (fromIntegral coord.row + 1/2) * tileSize - boardHeight/2
            }
          tileRect = Rectangle
            { rectangle'y = negate (tileSize / 2) + 1
            , rectangle'x = negate (tileSize/2) + 1
            , rectangle'width = tileSize - 2
            , rectangle'height = tileSize - 2
            }
      case tile ^. face of
        Shown -> do
          case tile ^. tileType of
            Mine -> drawRectanglePro tileRect (origin |* (-1)) 0 red
            _ -> pure ()
          drawText (showTile $ tile ^. tileType) (round (origin.vector2'x - tileSize/2)) (round (origin.vector2'y - tileSize/2)) (floor (tileSize / 6)) green
        Hidden -> drawRectanglePro tileRect (origin |* (-1)) 0 lightGray
        Flag -> do
          let triangleFactor = tileSize * 0.3
          drawRectanglePro tileRect (origin |* (-1)) 0 lightGray
          drawTriangle (origin |+| (Vector2 1 (-1) |* triangleFactor)) (origin |+| (Vector2 (-1) (-1) |* triangleFactor)) (origin |+| (Vector2 0 1 |* triangleFactor)) blue

withRenderTexture :: (MonadIO m, MonadMask m) => Int -> Int -> WindowResources -> (RenderTexture -> m b) -> m b
withRenderTexture width height windowResources =
  bracket
    (liftIO $ loadRenderTexture width height windowResources)
    (\text -> liftIO $ unloadRenderTexture text windowResources)
