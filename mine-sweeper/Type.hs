{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE OverloadedRecordDot #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE NoFieldSelectors #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE FunctionalDependencies #-}
module Type (module Type) where

import qualified Data.Vector as V
import Raylib.Types
import Control.Lens
import Data.IntSet (IntSet)
import qualified Data.IntSet as IntSet
import System.Random (Finite, Uniform, StdGen)
import GHC.Generics
import System.Random.Stateful (IOGenM)

data Coord = Coord { col :: Int, row :: Int }
  deriving stock (Show, Eq, Ord)
data TileType = Mine | Blank | Number Int
  deriving stock (Generic, Show, Eq)
  deriving anyclass (Finite, Uniform)

data Face = Hidden | Shown | Flag
  deriving stock (Generic, Show, Eq)

data Cond = Win | Lose | Playing
  deriving stock (Show, Eq)

data Difficulty = Beginner | Intermediate | Hard | Custom Coord Int
  deriving stock (Show)

diffDimension :: Difficulty -> Coord
diffDimension Beginner = Coord { row = 10, col = 10 }
diffDimension Intermediate = Coord { row = 16, col = 16 }
diffDimension Hard = Coord { row = 16, col = 30 }
diffDimension (Custom dim _) = dim

diffNumberOfMine :: Difficulty -> Int
diffNumberOfMine Beginner = 10
diffNumberOfMine Intermediate = 40
diffNumberOfMine Hard = 99
diffNumberOfMine (Custom _ mines) = mines

data Tile = Tile { _tileType :: TileType, _face :: Face }
  deriving stock Generic
makeFieldsNoPrefix ''Tile

newtype Grid a = Grid { toVector :: V.Vector (V.Vector a) }
  deriving stock (Functor, Foldable, Traversable, Show)

data GameState =
  GameState
    { _mousePosition :: Vector2
    , _camera :: Camera2D
    , _zoomFactor :: Float
    , _gameBoard :: Grid Tile
    , _gameSeed :: IOGenM StdGen
    , _gameDifficulty :: Difficulty
    , _winningState :: Cond
    , _timeElapsed :: Float
    , _adjacentTiles :: Coord -> [Coord]
    }
makeFieldsNoPrefix ''GameState

tileFromInt :: Int -> TileType
tileFromInt n | n <= 0 = Blank
tileFromInt n = Number n

showTile :: TileType -> String
showTile Mine = "mine"
showTile Blank = "blank"
showTile (Number n) = show n

data BoardSizes = BoardSizes { tileSize :: Float, boardWidth :: Float, boardHeight :: Float, boardRectangle :: Rectangle}

instance FunctorWithIndex Coord Grid where
  imap f (Grid g) = Grid $ V.imap (\row -> V.imap (\col -> f Coord { col, row })) g
instance FoldableWithIndex Coord Grid
instance TraversableWithIndex Coord Grid where
  itraverse f (Grid g) = Grid <$> traverse (\(row, rows) -> traverse (\(col, a) -> f (Coord { row, col }) a) (V.indexed rows)) (V.indexed g)

type instance Index (Grid a) = Coord
type instance IxValue (Grid a) = a

instance Ixed (Grid a) where
  ix (Coord {row, col}) f (Grid g) = Grid <$> ix row (ix col f) g

viewportResolution :: Vector2
viewportResolution = Vector2 600 400

viewportHeight, viewportWidth :: Float
Vector2 viewportWidth viewportHeight = viewportResolution

viewportRect :: Rectangle
viewportRect = let Vector2 w h = viewportResolution in Rectangle 0 0 w (-h)

maxBoardWidth :: Float
maxBoardWidth = viewportWidth - 100

maxBoardHeight :: Float
maxBoardHeight = viewportHeight - 100

gridDimension :: Grid a -> (Int, Int)
gridDimension (Grid g) = case V.length g of
  0 -> (0, 0)
  numberOfRow -> (numberOfRow, V.length (g V.! 0))

boardSizes :: Grid a -> BoardSizes
boardSizes board =
  let (numOfRow, numOfCol) = gridDimension board
      tileSize = min (maxBoardHeight / fromIntegral numOfRow) (maxBoardWidth / fromIntegral numOfCol)
      boardHeight = tileSize * fromIntegral numOfRow
      boardWidth = tileSize * fromIntegral numOfCol
      boardRectangle = Rectangle
        { rectangle'y = negate boardHeight/2
        , rectangle'x = negate boardWidth/2
        , rectangle'width = boardWidth
        , rectangle'height = boardHeight
        }
  in BoardSizes {tileSize, boardWidth, boardHeight, boardRectangle}

ordinals :: [Int] -> IndexedTraversal' Int (V.Vector a) a
ordinals is f v = (v V.//) <$> traverse (\i -> (,) i  <$> indexed f i (v V.! i)) (ordinalNub (V.length v) is)

-- | Return the the subset of given ordinals within a given bound and in order
-- of the first occurrence seen.
--
-- Bound: @0 <= x < l@
--
-- >>> ordinalNub 3 [-1,2,1,4,2,3]
-- [2,1]
ordinalNub ::
  Int   {- ^ strict upper bound -} ->
  [Int] {- ^ ordinals           -} ->
  [Int] {- ^ unique, in-bound ordinals, in order seen -}
ordinalNub l xs = foldr (ordinalNubHelper l) (const []) xs IntSet.empty

ordinalNubHelper :: Int -> Int -> (IntSet -> [Int]) -> (IntSet -> [Int])
ordinalNubHelper l x next seen
  | outOfBounds || notUnique = next seen
  | otherwise                = x : next (IntSet.insert x seen)
  where
  outOfBounds = x < 0 || l <= x
  notUnique   = x `IntSet.member` seen